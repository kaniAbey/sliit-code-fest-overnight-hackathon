﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Timers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SLIITWeb
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(Timer_Tick);
            timer.Enabled = true;
            timer.Interval = 2000;
            if (!this.IsPostBack)
            {
               
                this.BindGrid();
            }
        }

        private void Timer_Tick(object sender, ElapsedEventArgs e)
        {
            this.BindGrid();
        }

        private static SqlConnection Connection()
        {

            string sqlconn = @"Data Source=KANIABEY\SQLEXPRESS;Initial Catalog=SLIIT;Integrated Security=True;Pooling=False";
           SqlConnection con = new SqlConnection(sqlconn);

            return con;

        }

        private void InsertCSVRecords(DataTable csvdt)
        {
            SqlConnection con = new SqlConnection();
            con = Connection();
              
            SqlBulkCopy objbulk = new SqlBulkCopy(con);
            
            objbulk.DestinationTableName = "Shift_Info";
            objbulk.ColumnMappings.Add("Name", "Employee_Name");
            objbulk.ColumnMappings.Add("Manager", "Line_Manager");
            objbulk.ColumnMappings.Add("Shift", "Shift");
            con.Open();
            objbulk.WriteToServer(csvdt);
            con.Close();


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            
            DataTable tblcsv = new DataTable();
            tblcsv.Columns.Add("Employee_Name");
            tblcsv.Columns.Add("Line_Manager");
            tblcsv.Columns.Add("Shift");  
            string CSVFilePath = Path.GetFullPath(FileUpload1.PostedFile.FileName);
         
            string ReadCSV = File.ReadAllText(CSVFilePath);
            foreach (string csvRow in ReadCSV.Split('\n'))
            {
                if (!string.IsNullOrEmpty(csvRow))
                {
                      
                    tblcsv.Rows.Add();
                    int count = 0;
                    foreach (string FileRec in csvRow.Split(','))
                    {
                        tblcsv.Rows[tblcsv.Rows.Count - 1][count] = FileRec;
                        count++;
                    }
                }


            }
             
            InsertCSVRecords(tblcsv);
            Response.Write("<script LANGUAGE='JavaScript'>alert('Your CSV Has Been Submitted') </script>");

        }

        private void BindGrid()
        {
            SqlConnection Con = new SqlConnection();

            Con = Connection();
            using (Con)
            {
                using (SqlCommand cmd = new SqlCommand("select * from Shift_Info"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = Con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            CSVDatah.DataSource = dt;
                            CSVDatah.DataBind();
                        }
                    }
                }
            }
        }

        protected void PrintRpt_Click(object sender, EventArgs e)
        {
            SqlConnection con = Connection();
            DataSet ds = new DataSet();
          SqlDataAdapter  adp = new SqlDataAdapter("select * from Shift_Info", con);
            adp.Fill(ds);
            ReportDocument rd = new ReportDocument();
            rd.Load("~/CrystalReport1.rpt");
            rd.SetDataSource(ds.Tables[0]);
            CrystalReportViewer1.ReportSource = rd;
        }
    }
}