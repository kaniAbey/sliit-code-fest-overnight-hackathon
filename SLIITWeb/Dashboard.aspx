﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="SLIITWeb.Contact" %>


<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="w3-container">
        <br />
        <br />
        <br />
        <br />
        <p style="font-size: xx-large">
            Dashboard
        </p>

        <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1">
            <Titles>
                <asp:Title Text="Count by Step ID">
                </asp:Title>
            </Titles>
            <Series>
                <asp:Series Name="Series1" ChartType="Bar" XValueMember="ID1" YValueMembers="Count">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>

        <br />
    <br />
    <br />
    <br />
    <p style="font-size:xx-large">
        Deffective Items
    </p>
        <asp:GridView runat="server" ID="gridCount" AutoGenerateColumns="false" CssClass="w3-table-all w3-hoverable">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Step ID" />
                <asp:BoundField DataField="Count" HeaderText="Count" />
                <asp:BoundField DataField="Okay" HeaderText="Deffective Items" ItemStyle-ForeColor="Red" />
                <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:d}" />
            </Columns>

        </asp:GridView>

        <br />
    <br />
    <br />
    <br />
    <p style="font-size:xx-large">
        Speed
    </p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [ID1], [Date], [Okay], [Count] FROM [Productions]"></asp:SqlDataSource>

        <asp:Chart ID="Chart2" runat="server">
            <Series>
                <asp:Series ChartType="FastLine" Name="Series1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                    <AxisX>
                        <CustomLabels>
                            <asp:CustomLabel />
                        </CustomLabels>
                    </AxisX>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
</asp:Content>

