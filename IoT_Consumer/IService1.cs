﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace IoT_Consumer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    
    public interface IService1
    {
       

        [OperationContract]
        [SelfValidation]
        void IoTConsume(long ID, int Count, bool Okay);
        [OperationContract]
        [SelfValidation]
        List<IoTData> DataIngressed(long ID);

        [OperationContract]
        [SelfValidation]
        List<IoTData> GetData();
        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "IoT_Consumer.ContractType".
 
    [DataContract]
    public class IoTData
    {
        long Id;
        int count;
        bool okay;

        public IoTData() { }

        public IoTData(long Id, int count, bool okay) {


            this.Id = Id;
            this.count = count;
            this.okay = okay;

        }

        [DataMember]
        public long ID
        {
            get { return Id; }
            set { Id = ID; }
        }

        [DataMember]
        public int Count
        {
            get { return count; }
            set { count = Count; }
        }

        [DataMember]
        public bool Okay
        {
            get { return okay; }
            set { okay = Okay; }
        }
    }
}
