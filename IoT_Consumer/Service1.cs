﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Timers;

namespace IoT_Consumer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        public List<IoTData> list = new List<IoTData>();
        private  Timer timer = new Timer();

        public Service1()
        {
            timer.Elapsed += new ElapsedEventHandler(Timer_Tick);
            timer.Enabled = true;
            timer.Interval = 2000;
           
        }

        public void IoTConsume(long ID, int Count, bool Okay)
        {
            try
            {
                list.Add(new IoTData(ID, Count, Okay));
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public List<IoTData> DataIngressed(long ID)
        {
            try
            {
                return list.FindAll(x => x.ID == ID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<IoTData> GetData()
        {
            return list.ToList();
        }

        public void Timer_Tick(Object sender , System.Timers.ElapsedEventArgs e)
        {
            bool IsEmpty = !list.Any();
            if (IsEmpty)
            {

                timer.Interval++;

            }
            else
            {
                var Data = GetData();

                using (SqlConnection con = new SqlConnection(@"Data Source=KANIABEY\SQLEXPRESS;Initial Catalog=SLIIT;Integrated Security=True;Pooling=False"))
                    if (con.State == ConnectionState.Closed)


                    {
                        con.Open();
                       // SqlTransaction transaction = con.BeginTransaction();
                        string query1 = "INSERT INTO Productions (Id,Count,Okay,Date)";
                        query1 += "VALUES (@Id,@Count,@Okay,@Date)";
                        SqlCommand myCommand = new SqlCommand(query1, con);
                        myCommand.Parameters.AddWithValue("@Id", DbType.Int64);
                        myCommand.Parameters.AddWithValue("@Count", DbType.Int32);
                        myCommand.Parameters.AddWithValue("@Okay", DbType.Boolean);
                        myCommand.Parameters.AddWithValue("@Date", DateTime.Today.ToUniversalTime());


                        foreach (var item in Data)
                        {
                            myCommand.Parameters[0].Value = item.ID;
                            myCommand.Parameters[1].Value = item.Count;
                            myCommand.Parameters[2].Value = item.Okay;
                            myCommand.ExecuteNonQuery();
                            break;
                        }

                       // transaction.Commit();
                        con.Close();

                        timer.Interval = 9000;
                        list.Clear();
                    }

            }
        }


      
    }
}
